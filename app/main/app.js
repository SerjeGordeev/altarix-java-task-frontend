import angular from "angular";
import "angular-ui-router";
import "restangular";
import "ng-file-upload"
import "normalize.css";
import "styles/common.scss";

import "components/schools";
import "components/common";
import "components/backend";

import "@iamadamjowett/angular-click-outside/clickoutside.directive.js";
import "angular-aria";
import "angular-animate";
import "angular-material";
import "angular-material/angular-material.min.css";

const App = angular.module("altTask", [
	"ui.router",
	"altTask.common",
	"altTask.backend",
	"restangular",
    "altTask.schools",
	"angular-click-outside",
	"ngMaterial"
]);

App.config(function ($stateProvider, $locationProvider, $urlRouterProvider, flashAlertProvider, $mdThemingProvider) {
	$urlRouterProvider.otherwise("/schools/list");
	flashAlertProvider.setAlertTime(2000);
    $mdThemingProvider.definePalette("mcgpalette0", {
        "50": "edf0f5",
        "100": "d3dae7",
        "200": "b6c2d7",
        "300": "99aac7",
        "400": "8397bb",
        "500": "6d85af",
        "600": "657da8",
        "700": "5a729f",
        "800": "506896",
        "900": "3e5586",
        "A100": "dae5ff",
        "A200": "a7c2ff",
        "A400": "749fff",
        "A700": "5a8dff",
        "contrastDefaultColor": "light",
        "contrastDarkColors": [
            "50",
            "100",
            "200",
            "300",
            "400",
            "500",
            "A100",
            "A200",
            "A400",
            "A700"
        ],
        "contrastLightColors": [
            "600",
            "700",
            "800",
            "900"
        ]
    });

    $mdThemingProvider.theme("default")
        .primaryPalette("mcgpalette0")
});

App.run(($rootScope, $state, $document,  $window, $q)=>{
    $window.Promise = $q;
});