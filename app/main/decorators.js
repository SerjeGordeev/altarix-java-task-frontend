/**
 * Декоратор для компонента
 * @param options
 * @returns {Function}
 * @constructor
 */
export function Component(options) {
	return function (target) {
		return {...options, controller: target};
	};
}