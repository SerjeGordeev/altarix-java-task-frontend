import "./styles.scss";
import template from "./school-create-modal.template.html";
import {Component} from "main/decorators";

@Component({
    selector: "schoolCreateModal",
    template
})
class SchoolCreateModalComponent {
    constructor($mdDialog) {
        this.services = {$mdDialog};
        this.newSchool = {name: null};
    }


    cancel() {
        const {$mdDialog} = this.services;
        $mdDialog.cancel();
    }

    create() {
        const {$mdDialog} = this.services;
        $mdDialog.hide(this.newSchool);
    }
}

export {SchoolCreateModalComponent};