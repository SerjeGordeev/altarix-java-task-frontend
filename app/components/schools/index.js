import {SchoolsListComponent} from "./schools-list/schools-list.component";
import {SchoolsListItemComponent} from "./schools-list-item/schools-list-item.component";
import {SchoolCreateModalComponent} from "./school-create-modal/school-create-modal.component";
import {SchoolDetailComponent} from "./school-detail/school-detail.component";


angular
	.module("altTask.schools", [])
	.component(SchoolsListComponent.selector, SchoolsListComponent)
	.component(SchoolsListItemComponent.selector, SchoolsListItemComponent)
	.component(SchoolCreateModalComponent.selector, SchoolCreateModalComponent)
	.component(SchoolDetailComponent.selector, SchoolDetailComponent)
	.config(route);

function route($stateProvider) {

	$stateProvider.state({
		name: 'schools',
		url: '/schools',
		template: `<ui-view></ui-view>`,
		onEnter(){}
	});

    $stateProvider.state({
        name: 'schools.list',
        url: '/list',
        template: `<schools-list></schools-list>`,
        onEnter(){}
    });

    $stateProvider.state({
        name: 'schools.detail',
        url: '/list/:id',
        template: `<school-detail></school-detail>`,
        params: {
            id: null
        }
    });

/*	$stateProvider.state({
		name: 'movies.list',
		url: '/list',
		template: `<schools-list></schools-list>`,
		onEnter: function(){

		}
	});

	$stateProvider.state({
		name: 'movies.create',
		url: '/create',
		template: `<schools-editor></schools-editor>`,
		onEnter: function(){

		}
	});

	$stateProvider.state({
		name: 'movies.edit',
		url: '/list/:id/edit',
		template: `<schools-editor></schools-editor>`,
		onEnter: function(){

		},
		params: {
			id: null,
			movieData: null
		}
	});

	});*/
}