import "./styles.scss";
import template from "./schools-list-item.template.html";
import {Component} from "main/decorators";
import starIcon from "images/ic_star_white_24px.svg";
import removeIcon from "images/ic_black_24px .svg";

@Component({
	selector: "schoolsListItem",
	template,
	bindings: {
		schoolData: "<",
		onRemove: "&"
	}
})
class SchoolsListItemComponent {

	/* @ngInject */
	constructor() {
		this.icons = {starIcon, removeIcon};
	}

	removeMovie(movie, ev){
		ev.stopPropagation();
		this.onRemove({movie});
	}
}

export {SchoolsListItemComponent};
