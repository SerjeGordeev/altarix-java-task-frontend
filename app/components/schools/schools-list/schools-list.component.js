import "./styles.scss";
import template from "./schools-list.template.html";
import {Component} from "main/decorators";
import {SchoolCreateModalComponent} from "components/schools/school-create-modal/school-create-modal.component";

import addIcon from "images/ic_add_black_24px.svg";

@Component({
    selector: "schoolsList",
    template
})
class SchoolsListComponent {

    /* @ngInject */
    constructor($state, Schools, $mdDialog, flashAlert) {
        this.services = {$state, Schools, $mdDialog, flashAlert};
        this.icons = {addIcon};
        this.schools = [];
    }

    async $onInit() {
        const {Schools} = this.services;

        this.schools = await (this.promise = Schools.api().getList({minify: true}));
    }

    openCreateModal() {
        const {$mdDialog} = this.services;

        $mdDialog.show({
            controller: SchoolCreateModalComponent.controller,
            controllerAs: "$ctrl",
            template: SchoolCreateModalComponent.template,
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            focusOnOpen: false
        }).then(this.createSchool.bind(this)).catch();
    }

    openDetail(school) {
        const {$state} = this.services;
        $state.go("schools.detail", {id: school.id});
    }

    async removeMovie(school, ix) {
        const {flashAlert} = this.services;
        try {
            await (this.promise = school.remove());
            this.schools.splice(ix, 1)
            flashAlert.success("Сущность школы успешно удалена");
        } catch (er) {
            flashAlert.error("Не удалось удалить сущность школы");
        }
    }

    async createSchool(school) {
        const {Schools, flashAlert} = this.services;
        await (this.promise = Schools.api().post(school));
        await this.$onInit();
        flashAlert.success("Сущность школы успешно создана");
    }
}

export {SchoolsListComponent};
