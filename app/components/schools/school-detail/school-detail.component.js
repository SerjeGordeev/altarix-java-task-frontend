import "./styles.scss";
import template from "./school-detail.template.html";
import {Component} from "main/decorators";

@Component({
	selector: "schoolDetail",
	template
})
class SchoolDetailComponent {

	/* @ngInject */
	constructor($state, $stateParams, Schools, $scope) {
		this.services = {$state, $stateParams, Schools, $scope};
        this.editMode = false;
	}

	$onInit(){
		if(!this.schoolData){
			this.promise = this.getInitialMovie();
		}
	}

	async getInitialMovie(){
		const {Schools, $stateParams, $scope} = this.services;

        this.schoolData = await (this.promise = Schools.api().one($stateParams.id).get());
	}

    toggleEditMode() {
		this.editMode = !this.editMode;
	}

    openClassUnitDetail(classUnit) {

	}
}

export {SchoolDetailComponent};
