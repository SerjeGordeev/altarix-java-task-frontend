import "./styles.scss";
import _ from "lodash";
import template from "./app-header.template.html";
import {Component} from "main/decorators";

@Component({
	selector: "appHeader",
	template
})
class AppHeaderComponent {

	/* @ngInject */
	constructor() {
		this.services = {};
	}

}

export {AppHeaderComponent};
