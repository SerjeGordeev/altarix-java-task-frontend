import "./styles.scss";
import _ from "lodash";
import template from "./app-checkbox.html";
import {Component} from "main/decorators";

@Component({
	selector: "appCheckbox",
	template,
	bindings: {
		itemValue: "<",
		itemTitle: "@",
		isDisabled: "<",
		isSelected: "<",
		onToggle: "&",
		options: "<?" /* {hint, required: {enabled, text}, warning: {enabled, text}} */
	}
})
export class AppCheckboxComponent {
	constructor() {
		this.checked = false;
		this.hasError = false;
		this.hasWarning = false;
		this.showingHelpBlock = false;
		this.helpBlockText = "";
	}

	$onInit() {
		this.$setChecked();
	}

	$onChanges(changes) {
		if (changes.isSelected) {
			this.$setChecked();
		}
	}

	toggle(event) {
		event.preventDefault();
		event.stopPropagation();

		if (this.isDisabled) {
			return;
		}

		this.checked = !this.checked;
		this.onToggle({value: this.itemValue});
	}

	$setChecked() {
		this.checked = this.isSelected || false;
	}
}