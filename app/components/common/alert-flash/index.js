
import "./alert.scss";

angular.module("altTask.common").constant("alertConfig", {
  success: "alert-success",
  error: "alert-danger",
  info: "alert-info"
}).provider("flashAlert", function() {
  var a = [],
      b = 5e3;
  return {
    setAlertTime: function(a) {
      b = a
    },
    $get: ["$timeout", "alertConfig", function(tm, d) {
      return {
        success: function(a) {
          this.add("success", a)
        },
        error: function(a) {
          this.add("error", a)
        },
        info: function(a) {
          this.add("info", a)
        },
        getAlert: function() {
          return a
        },
        add: function(b, c) {
          var e = {
            typeOfAlert: d[b],
            msg: c,
            remove: function(){
              a.splice(0 ,1)
            }
          };

          tm(a.push(e));
          this.hideAlert(e)
        },
        hideAlert: function() {
            tm(function() {
            a.shift()
          }, b)
        }
      }
    }]
  }
}).directive("alertFlash", ["flashAlert", function(a) {
  return {
    restrict: "E",
    template: require("./alert.html"),
    scope: {},
    link: function(b) {
      b.$watch(a.getAlert, function() {
        b.alerts = a.getAlert()
      })
    }
  }
}]);

class alertFlashDirective{
  constructor(){
    this.restrict = "E";
    this.template = require("./alert.html");
    this.scope = {};
  }

  link(b) {
    b.$watch(a.getAlert, function() {
      b.alerts = a.getAlert()
    })
  }
}
