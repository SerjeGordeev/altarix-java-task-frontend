import "./styles.scss";
import _ from "lodash";
import template from "./editable-list.template.html";
import {Component} from "main/decorators";

import addIcon from "images/ic_add_black_24px.svg";
import removeIcon from "images/ic_black_24px .svg";

@Component({
	selector: "editableList",
	template,
	bindings: {
		listData: "<",
		onRemove: "&",
		onCreate: "&",
		onChange: "&",
		options: "<"
	}
})
class EditableListComponent {

	/* @ngInject */
	constructor() {
		this.icons = {addIcon, removeIcon};
	}

	getLabel(item){
		if(this.options.getLabel) {
			return this.options.getLabel(item);
		}

		return item;
	}

	createNewItem(){
		this.onCreate();
	}
}

export {EditableListComponent};
