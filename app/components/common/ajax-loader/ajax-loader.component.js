import "./styles.scss";
import template from "./ajax-loader.html";
import {Component} from "main/decorators";

@Component({
    selector: "ajaxLoader",
    template,
    bindings: {
        promise: "<"
    }
})
class AjaxLoaderComponent {

    constructor($timeout) {
        this.services = {$timeout};
        /*
         promise.$$state.status === 0 // pending
         promise.$$state.status === 1 // resolved
         promise.$$state.status === 2 // rejected
         */
        this.state = 1;
    }

    $onChanges() {
        const {$timeout} = this.services;
        if (!this.promise) {
            this.state = 1;

            return;
        }

        this.state = 0;
        this.promise
            .then(() => {
                this.state = 1;
            })
            .catch(() => {
                this.state = 2;
            }).finally(() => {
            $timeout(this.state = 1);
        })
    }
}

export {AjaxLoaderComponent};