class AppModalService {
    /* @ngInject */
    constructor($q, $document, $compile, $scope){
      this.services = {$q, $document, $compile, $scope};
      console.log($document)
    }

    open(config) {
        const {$compile, $scope} = this.services;
        const scope = $scope.$new();

        const modalTemplate = `<div class="app-modal">${config.template}</div>`;
        scope.data = data;
        data.element = angular.element($compile(modalTemplate)(scope));

        angular
            .element(document.querySelector("body"))
            .append(data.element);
    }
}
export {AppModalService};