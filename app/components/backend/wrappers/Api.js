/**
 * @ngInject
 */
function Api(Restangular) {
	return Restangular.withConfig((RestangularConfigurer) => {
		RestangularConfigurer.setDefaultHeaders({Accept: "application/json"});
		RestangularConfigurer.setBaseUrl("/api");
	});
}

export {Api};