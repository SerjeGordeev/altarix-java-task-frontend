import { SchoolsService } from "./services/schools.service.js";
import { ClassUnitsService } from "./services/classUnits.service.js";
import { StudentsService } from "./services/students.service.js";
import { SubjectsService } from "./services/subjects.service.js";
import { TeachersService } from "./services/teachers.service.js";
import { JournalsService } from "./services/journals.service.js";
import { MarksService } from "./services/marks.service.js";

import { Api } from "./wrappers/Api.js";

angular
	.module("altTask.backend",[])
	.service("Schools", SchoolsService)
	.service("ClassUnits", ClassUnitsService)
	.service("Students", StudentsService)
	.service("Subjects", SubjectsService)
	.service("Teachers", TeachersService)
	.service("Journals", JournalsService)
	.service("Marks", MarksService)

	.factory("Api", Api);
