class SubjectsService {
	/* @ngInject */
	constructor(Api, $q){
		this.services = {Api, $q};
		this.apiUrl = "/subjects";
	}

	api() {
		const {Api} = this.services;

		return Api.service(this.apiUrl);
	}

}
export {SubjectsService};