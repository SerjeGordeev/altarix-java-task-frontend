class JournalsService {
	/* @ngInject */
	constructor(Api, $q){
		this.services = {Api, $q};
		this.apiUrl = "/journals";
	}

	api() {
		const {Api} = this.services;

		return Api.service(this.apiUrl);
	}

}
export {JournalsService};