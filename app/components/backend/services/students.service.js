class StudentsService {
	/* @ngInject */
	constructor(Api, $q){
		this.services = {Api, $q};
		this.apiUrl = "/students";
	}

	api() {
		const {Api} = this.services;

		return Api.service(this.apiUrl);
	}

}
export {StudentsService};