class SchoolsService {
	/* @ngInject */
	constructor(Api, $q){
		this.services = {Api, $q};
		this.apiUrl = "/schools";
	}

	api() {
		const {Api} = this.services;

		return Api.service(this.apiUrl);
	}

}
export {SchoolsService};