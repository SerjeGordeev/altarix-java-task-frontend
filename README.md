**Neotek-task-frontend**

Фронтенд приложения каталога фильмов.

**Сборка и запуск**

    npm i - установка зависимостей

    npm run watch:heroku - запуск webpack-dev-server
     с проксированием запросов к http://neotek-task.herokuapp.com/

    npm run watch:localhost - запуск webpack-dev-server
          с проксированием запросов к локальному серверу приложения,
          который должен быть предварительно запущен на 8780 порту
          репозиторий серверной части - https://gitlab.com/SerjeGordeev/neotek-task-backend.git
     
    npm run build - сборка бандла приложения


